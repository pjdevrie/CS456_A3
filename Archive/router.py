from socket import *
from struct import *
from copy import copy
import sys

#
# global variables
#
# command line parameters
global router_id
global nse_host
global nse_port
global router_port

# which routers have send Hello Packets
global hello_from
hello_from = []

# which packets have been received by the router
global received_LSPDUs
received_LSPDUs = []

# log file
global segnum_file

# link state DB
global link_state_DB
link_state_DB = [ None, None, None, None, None ]

# number of routers
NBR_ROUTER = 5

# large number
global router_info
INF = 65535



#
# Classes
#
class pktHELLO():
    def __init__(self, router_id, link_id):
        self.router_id = router_id
        self.link_id = link_id

class pktLSPDU():
    def __init__(self, sender, router_id, link_id, cost, via):
        self.sender = sender
        self.router_id = router_id
        self.link_id = link_id
        self.cost = cost
        self.via = via

class pktINIT():
    def __init__(self, router_id):
        self.router_id = router_id

    def UDPPacket(self):
        return pack('<i', self.router_id )

class linkCost():
    def __init__(self, link, cost):
        self.link = link
        self.cost = cost

class circuitDB():
    def __init__(self, nbr_link, linkcosts):
        self.nbr_link = nbr_link
        self.linkcosts = linkcosts
        #struct link_cost linkcost[NBR_ROUTER];



# custom class used in shortest path algorithm
class routerStats():
    def __init__( self, router_number, distance, previous ):
        self.router_number = router_number
        self.distance = distance
        self.previous = previous



# start of program
def init():
    if ( len(sys.argv) < 5 ):
        print "Too few command-line parameters! Exiting now!"
        sys.exit
        return
    elif ( len(sys.argv) > 5 ):
        print "Too many command-line paramters! Exiting now!"
        sys.exit
        return

    #
    # read command-line parameters
    #
    global router_id
    router_id = int(sys.argv[1])
    global nse_host
    nse_host = sys.argv[2]
    global nse_port
    nse_port = int(sys.argv[3])
    global router_port
    router_port = int(sys.argv[4])

    # open log file
    global segnum_file
    segnum_file = open( "router" + str(router_id) + ".log", "w+")

    # initialize send socket and bind
    global nse_socket
    nse_socket = socket(AF_INET, SOCK_DGRAM)
    nse_socket.bind(( "ubuntu1604-004.student.cs.uwaterloo.ca", router_port ))

    # send init packet to server
    nse_socket.sendto( pack('<I', router_id ), (nse_host, nse_port))
    print "See logs for details..."
    segnum_file.write( "R" + str( router_id ) + " sends an INIT: router_id " \
                      + str( router_id ) + "\n" )

    # move to next stage
    listenCircuitDB()



# receive circuit DB packet
def listenCircuitDB():
    listen = None

    # listen for circuit DB
    listen, clientAddress = nse_socket.recvfrom( 1024 )
    # parse packet - little endian and 11 unsigned integers = 44 bytes
    circuit_DB = unpack( '<11I', listen )

    # number of links
    nbr_links = circuit_DB[0]

    # log message to file
    segnum_file.write( "R" + str( router_id ) + " receives a CIRCUIT_DB: nbr_link " \
                      + str( nbr_links ) + "\n" )

    link_costs = []
    i = 1
    # store circuit DB
    for l in range( nbr_links ):
        link = circuit_DB[ i ]
        cost = circuit_DB[ i + 1 ]
        link_cost = linkCost( link, cost )
        link_costs.append( link_cost )
        i = i + 2

    initCircuitDBS()

    # initialize circuitDB
    circuit_DB = circuitDB( nbr_links, link_costs )
    link_state_DB[ router_id - 1 ] = circuit_DB

    # print topology for first time
    printTopology()

    # move to next stage
    sendHello( circuit_DB )



# send pktHello
def sendHello( circuit_DB ):
    link_costs = circuit_DB.linkcosts

    # send hello packet across all links
    for l_c in link_costs:
        link_id = l_c.link

        # hello_packet: pktHELLO( router_id, link_id )
        # send hello packet - little endian, 2 unsigned integers = 8 bytes
        nse_socket.sendto( pack('<2I', router_id, link_id ), (nse_host, nse_port))

        # log message to file
        segnum_file.write( "R" + str( router_id ) + " sends a HELLO: router_id " \
                          + str( router_id ) + " link_id " + str( link_id ) + "\n" )

    # move to next stage
    receivePackets( circuit_DB )



# receiving Hello and LSPDU packets
def receivePackets( circuit_DB ):

    while 1:
        # listen for hello packet
        listen, clientAddress = nse_socket.recvfrom( 1024 )

        # received a Hello packet - size 45
        if ( sys.getsizeof( listen ) == 45 ):
            # parse packet - little endian, 2 unsigned integers = 8 bytes
            hello_packet = unpack( '<II', listen )
            from_router = hello_packet[0]
            from_link = hello_packet[1]

            # log message to file
            segnum_file.write( "R" + str( router_id ) + " receives a HELLO: router_id " \
                              + str( from_router ) + " link_id " + str( from_link ) + "\n" )

            # keep track of which links the router received hello packet from
            hello_from.append( from_link )

            # reply to router with pktLSPDU
            sendOwnLSPDU( circuit_DB, from_router )

        # received an LSPDU packet
        else:
            # parse packet - little endian, 5 unsigned integers = 20 bytes
            LSPDU_packet = unpack( '<5I', listen )

            # if this is not a duplicate packet, resend to neighbours
            if ( LSPDU_packet not in received_LSPDUs ):
                # add packet to list of received packets
                received_LSPDUs.append( LSPDU_packet )
                # propagate packet to other neighbours
                sendOtherLSPDUs( circuit_DB, LSPDU_packet )



# initialize and send own LSPDU packets (after receiving a HELLO packet)
def sendOwnLSPDU( circuit_DB, via ):
    # send information about each connected link through via (link where Hello
    # packet was received from )
    link_costs = circuit_DB.linkcosts
    for l_c in link_costs:
        sender = router_id
        link_id = l_c.link
        link_cost = l_c.cost

        # LSPDU_packet: pktLSPDU( sender, router_id, link_id, link_cost, via )
        # send LSPDU packet - little endian, 5 unsigned integers = 20 bytes
        nse_socket.sendto( pack('<5I', sender, router_id, link_id, link_cost, via ), \
                          (nse_host, nse_port))

        # log message to file
        segnum_file.write( "R" + str( router_id ) \
                          + " sends an LS PDU: sender " + str( sender ) \
                          + " router_id " + str( router_id ) \
                          + " link_id " + str( link_id ) \
                          + " cost " + str( link_cost ) \
                          + " via " + str( via ) + "\n" )



# resend received LSPDU packets
def sendOtherLSPDUs( circuit_DB, LSPDU_packet ):
    # LSPDU_packet fields
    sender = LSPDU_packet[0]
    rout_id = LSPDU_packet[1]
    link_id = LSPDU_packet[2]
    link_cost = LSPDU_packet[3]
    via = LSPDU_packet[4]

    # log message to file
    segnum_file.write( "R" + str( router_id ) + \
                      " receives an LS PDU: sender " + str( sender ) \
                      + " router_id " + str( rout_id ) \
                      + " link_id " + str( link_id ) \
                      + " cost " + str( link_cost ) \
                      + " via " + str( via ) + "\n" )

    # update topology
    updateTopology( LSPDU_packet )

    # update sender
    new_sender = router_id
    link_costs = circuit_DB.linkcosts
    # resend across all links ( with certain exceptions )
    for l_c in link_costs:
        new_via = l_c.link
        # only resend across new link where hello message came from
        if ( new_via != via and new_via in hello_from ):
            # LSPDU_packet: pktLSPDU( sender, router_id, link_id, link_cost, via )
            # send LSPDU packet - little endian, 5 unsigned integers = 20 bytes
            nse_socket.sendto( pack('<5I', new_sender, rout_id, link_id, link_cost, new_via ), \
                              (nse_host, nse_port))

            # log message to file
            segnum_file.write( "R" + str( router_id ) + \
                              " sends an LS PDU: sender " + str( new_sender ) + \
                              " router_id " + str( rout_id ) + \
                              " link_id " + str( link_id ) + \
                              " cost " + str( link_cost ) + \
                              " via " + str( new_via ) + "\n" )



# add a link to the topology and print out new topology
def updateTopology( LSPDU_packet ):
    # LSPDU_packet fields
    sender = router_id
    rout_id = LSPDU_packet[1]
    link_id = LSPDU_packet[2]
    link_cost = LSPDU_packet[3]
    via = LSPDU_packet[4]

    circuit_DB = link_state_DB[ rout_id - 1 ]
    # check that the link is not already stored
    for l_c in circuit_DB.linkcosts:
        # if link is already stored, don't update topology - STOP!
        if ( l_c.link == link_id ):
            return

    # add link to topology for the router
    circuit_DB.nbr_link = circuit_DB.nbr_link + 1
    new_link_cost = linkCost( link_id, link_cost )
    circuit_DB.linkcosts.append( new_link_cost )

    # always print topology after updating
    printTopology()



def printTopology():
    # print out topology
    segnum_file.write( "\n# Topology Database\n" )
    current_router = 1
    for c_DB in link_state_DB:
        # if no known links at a router, don't print
        if ( c_DB.nbr_link == 0 ):
            current_router = current_router + 1
            continue

        # print line number of links at the router
        segnum_file.write( "R" + str( router_id ) + " -> R" + str( current_router ) \
                          + " nbr link " + str( c_DB.nbr_link ) + "\n" )

        # print line for each link at the router
        for l_cost in c_DB.linkcosts:
            segnum_file.write( "R" + str( router_id ) + " -> R" + str( current_router ) \
                              + " link " + str( l_cost.link ) \
                              + " cost " + str( l_cost.cost ) + "\n" )

        # go to next router in topology
        current_router = current_router + 1

    # print extra line for readability after topology
    segnum_file.write( "\n" )

    findShortestPath()



# for each router, set up a circuit_DB in the link_state_DB
def initCircuitDBS():
    for i in range( NBR_ROUTER ):
        link_costs = []
        circuit_DB = circuitDB( 0, link_costs )
        link_state_DB[ i ] = circuit_DB



# calculate the shortest path to each router - using Dijkstra's algorithm
def findShortestPath():

    # list of unreached routers - initially all routers
    routers = range( NBR_ROUTER )
    global router_info
    router_info = []

    # initialze router stats
    distance = INF
    previous = None
    for i in range( NBR_ROUTER ):
        router_stats = routerStats( i, distance, previous )
        router_info.append( router_stats )

    # distance of starting router is 0
    router_info[ router_id - 1 ].distance = 0
    router_info[ router_id - 1 ].previous = "Local"

    # while still elements in vertex_set
    while routers:
        # current_router = router with least distance
        least_distance = INF
        current_router = routers[0]
        for r in routers:
            r = router_info[ r ]
            if ( r.distance < least_distance ):
                least_distance = r.distance
                current_router = r.router_number

        # remove router from list of unreached routers
        routers.remove( current_router )
        router_circuit_DB = copy( link_state_DB[current_router] )

        # check all neighbouring links
        for l_c in router_circuit_DB.linkcosts:
            # prospective route has distance = distance of current router
            # + cost of travelling across link
            prospective_dist = router_info[ current_router ].distance + l_c.cost

            # find neighbouring router
            neighbour_router = routerWithLink( l_c.link, current_router )

            # if no other known router is connected to the link, skip iteration
            if ( neighbour_router == INF ):
                continue
            else:
                neighbour_info = router_info[ neighbour_router ]
                # if prospective distance to neighbouring router is less than
                # previous distance to that router, update
                if ( prospective_dist < neighbour_info.distance ):
                    neighbour_info.distance = prospective_dist
                    neighbour_info.previous = current_router

    printShortestPath( router_info )



def printShortestPath( router_info ):
    # print RIB
    segnum_file.write( "# RIB\n" )

    for router in router_info:
        # find path
        path = findPrevious( router_info, router.router_number, router.previous )

        segnum_file.write( "R" + str( router_id ) + " -> R" \
                          + str( router.router_number + 1 ) \
                          + " -> " + path + ", " \
                          + str( router.distance ) + "\n" )

    # print extra line for readability after RIB
    segnum_file.write( "\n" )



# find the path to take from current router_id, by tracing back through prev
def findPrevious( router_info, current, previous ):
    # if cannot trace back to router_id
    if ( previous is None ):
        return "INF"
    # if started trace at router_id
    elif ( previous == "Local" ):
        return "Local"
    else:
        # iterate back until you reach router_id
        while ( previous + 1 != router_id ):
            current = previous
            previous = router_info[ previous ].previous

        return "R" + str( current + 1 )

# returns the router_id that has link_id ( and is not 'not_router_id' )
def routerWithLink( link_id, not_router_id ):
    current_router = 0
    # check each circuit_DB in the link_state_DB
    for circuit_DB in link_state_DB:
        # if current router does not match not_router_id
        if ( current_router != not_router_id ):
            # check each link from a circuit_DB
            for l_c in circuit_DB.linkcosts:
                # if found link, return it
                if ( l_c.link == link_id ):
                    return current_router
        # incr router
        current_router = current_router + 1

    return INF



# start program
init()
