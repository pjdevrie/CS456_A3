Course: CS456
Assignment: 3
Student Name: Philip deVries
ID#: 20532498

Programming Language: Python 2.7.12 (on the student environment)

Running the program
  - run the following command on the nse_host:
    "./nselinux386 <routers_host> <nse_port>""
  - for each router_id, run the following command on the same routers_host:
    "python router.py <router_id> <nse_host> <nse_port> <router_port>"
  - no make file or command needed

Notes about the program:
- after running each router program, you will have to manually terminate
- you can use the nse print logs to determine when the last packets were redirected
- see each individual routerX.log file for each router instance that was run
- log messages should follow the format specified in the Assignment 3
  Description and FAQ files
- Topology is updated after NEW information arrives. RIB is calculated after
  each topology update. Both are printed in the log files back-to-back after
  each topology update
- each log file's last topology entry should match
- INF was used for unreachable router paths and 65535 was used for cost for
  unreachable paths
- see router.py file comments for program details
